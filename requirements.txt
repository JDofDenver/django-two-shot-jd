# asgiref==3.6.0
# black==23.1.0
# click==8.1.3
# Django==4.1.7
# djlint==1.19.1
# flake8==6.0.0
# mccabe==0.7.0
# mypy-extensions==1.0.0
# packaging==23.0
# pathspec==0.11.0
# platformdirs==3.0.0
# pycodestyle==2.10.0
# pyflakes==3.0.1
# sqlparse==0.4.3
# tomli==2.0.1

asgiref==3.5.2
black==22.10.0
click==8.1.3
colorama==0.4.5
cssbeautifier==1.14.6
Django==4.1.2
djlint==1.19.1
EditorConfig==0.12.3
flake8==5.0.4
html-tag-names==0.1.2
html-void-elements==0.1.0
importlib-metadata==5.0.0
jsbeautifier==1.14.6
mccabe==0.7.0
mypy-extensions==0.4.3
pathspec==0.10.1
platformdirs==2.5.2
pycodestyle==2.9.1
pyflakes==2.5.0
PyYAML==6.0
regex==2022.9.13
six==1.16.0
sqlparse==0.4.3
tomli==2.0.1
tqdm==4.64.1
zipp==3.9.0
